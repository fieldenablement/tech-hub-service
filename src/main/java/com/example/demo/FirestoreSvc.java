
package com.example.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import javax.annotation.Nullable;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.util.ExceptionUtils;

import com.google.api.core.ApiFuture;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.DocumentChange;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.EventListener;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.FirestoreException;
import com.google.cloud.firestore.FirestoreOptions;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
/**
 * A simple Quick start application demonstrating how to connect to Firestore
 * and add and query documents.
 */
public class FirestoreSvc {
	
	private Set<String> clientTokenStore = new HashSet<String>();

	private Firestore db;
	private JobMgmtSeviceClient jobMgmtSeviceClient;
	private static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
	private static final String SERVICEACCOUNTKEY = "/techhubpoc-firebase-adminsdk-bnfp4-e0f25408b9.json";

	public FirestoreSvc() throws IOException {
		//FileInputStream serviceAccount =
		//		new FileInputStream("path/to/techhubpoc-firebase-adminsdk-bnfp4-e0f25408b9.json");

		try (InputStream serviceAccountKey = this.getClass().getResourceAsStream(SERVICEACCOUNTKEY)) {
			FirebaseOptions options = new FirebaseOptions.Builder()
					.setCredentials(GoogleCredentials.fromStream(serviceAccountKey))
					.setDatabaseUrl("https://techhubpoc.firebaseio.com")
					.build();

			FirebaseApp.initializeApp(options);
			db = FirestoreClient.getFirestore();
			jobMgmtSeviceClient = new JobMgmtSeviceClient();
			addListeners();
		}
	}

	public FirestoreSvc(String projectId) throws FileNotFoundException, IOException {
		GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
		/*
	  FirebaseOptions options = new FirebaseOptions.Builder()
	      .setCredentials(credentials)
	      .setProjectId(projectId)
	      .build();
	  FirebaseApp.initializeApp(options);
	  db = FirestoreClient.getFirestore();
		 */
		FirestoreOptions options =
				FirestoreOptions.newBuilder().setTimestampsInSnapshotsEnabled(true)
				.setCredentials(credentials)
				.setProjectId(projectId)
				.build();
		Firestore firestore = options.getService();
		this.db = firestore;
		jobMgmtSeviceClient = new JobMgmtSeviceClient();
		addListeners();
	}



	public void add(Assignment assignment) throws InterruptedException, ExecutionException {
		// String scehduleDate = format.format(assignment.getStart().toDate());
		DocumentReference docRef = db.collection("assignments").document(assignment.getAssigmentId());
		Map<String, Object> data = new HashMap<>();
		data.put("classificationCd", assignment.getClassificationCd());
		data.put("jobId", assignment.getJobId());
		data.put("jobTypeCd", assignment.getJobTypeCd());
		data.put("assigmentId",assignment.getAssigmentId());
		data.put("locationAddressTxt", assignment.getLocationAddressTxt());
		data.put("productCategoryCd", assignment.getProductCategoryCd());
		data.put("serviceClassCd", assignment.getServiceClassCd());
		data.put("statusCd", assignment.getStatusCd());
		data.put("teamWorkerId", assignment.getTeamWorkerId());
		data.put("workOrderId", assignment.getWorkOrderId());
		ApiFuture<WriteResult> result = docRef.set(data);
		System.out.println("Update time : " + result.get().getUpdateTime());
		String message = "Job " + assignment.getAssigmentId() + " is Received!";
		this.clientTokenStore.forEach(e -> {
			try {
				NotificationPublisher.setNotification(message, e);
			} catch (ClientProtocolException e1) {
				e1.printStackTrace(System.out);
			} catch (IOException e1) {
				e1.printStackTrace(System.out);
			}
		});
		
	}


	private void addListeners() {
		System.out.println("Adding Listener !");
		db.collection("messages")
		.addSnapshotListener(new EventListener<QuerySnapshot>() {
			@Override
			public void onEvent(@Nullable QuerySnapshot snapshots,
					@Nullable FirestoreException e) {
				System.out.println("A Change in messages received");
				if (e != null) {
					System.err.println("Listen failed: " + e);
					return;
				}
				for (DocumentChange dc : snapshots.getDocumentChanges()) {
					switch (dc.getType()) {
					case ADDED:
						try {
							System.out.println("Added message: " + dc.getDocument().getData());
							updateAssignment(dc.getDocument().getData());
						} catch (Exception e1) {
							e1.printStackTrace(System.out);
						}
						break;
					case MODIFIED:
						System.out.println("Modified request: " + dc.getDocument().getData());
						break;
					case REMOVED:
						System.out.println("Removed request: " + dc.getDocument().getData());
						break;
					default:
						break;
					}
				}
			}
		});
	}

	private void updateAssignment(Map<String, Object> data) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, Exception {
		String jobId = (String)data.get("jobId");
		String assigmentId = (String)data.get("assigmentId");
		String teamWorkerId = (String)data.get("teamWorkerId");
		String resp = jobMgmtSeviceClient.acceptJob(jobId, assigmentId, teamWorkerId);
		
		DocumentReference docRef = db.collection("messages").document(assigmentId);
		Map<String, Object> respData = new HashMap<>();
		respData.put("notificationTxt", resp);
		ApiFuture<WriteResult> result = docRef.update(respData);
		System.out.println("Update time : " + result.get().getUpdateTime());
		
	}
	
	
	public void addNewClientMessageToken(String token) {
		this.clientTokenStore.add(token);
	}
	
	public void clearToken() {
		this.clientTokenStore.clear();
	}

	public JobMgmtSeviceClient getJobMgmtSeviceClient() {
		return this.jobMgmtSeviceClient;
	}

	/*
  public void read() throws InterruptedException, ExecutionException {
	      ApiFuture<QuerySnapshot> query =
	        db.collection("users").whereLessThan("born", 1900).get();
	    QuerySnapshot querySnapshot = query.get();
	    List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();
	    for (QueryDocumentSnapshot document : documents) {
	      System.out.println("User: " + document.getId());
	      System.out.println("First: " + document.getString("first"));
	      if (document.contains("middle")) {
	        System.out.println("Middle: " + document.getString("middle"));
	      }
	      System.out.println("Last: " + document.getString("last"));
	      System.out.println("Born: " + document.getLong("born"));
	    }
	  }
	 */
}
