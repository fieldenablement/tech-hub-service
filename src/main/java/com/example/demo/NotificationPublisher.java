package com.example.demo;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

public class NotificationPublisher {

	private static final String SERVER_KEY = "AAAAFtnQS3o:APA91bH1ajh25FPfDrSsjlqjSOvTVxDoFTjD3ZelsyPD3lPoOoXiGcmgLypoW9qSTpmFqmLkJd5ZWbeE4rm8toy9gWF9lkb65zkNfCV2opFcLJYY3XZ5k7kG4JH0nxOsncQmnLLf6_ZC";
	private static final String TEMPLATE = "{\"notification\": {"+
			"\"title\": \"Firebase\","+
			"\"body\": \"%s\", \"icon\": \"https://techhubpoc.firebaseapp.com/telus.png\"}, \"to\": \"%s\"}";
	
	private static final int CONNECTION_TIMEOUT_MS = 60000;
	private static final String ENDPOINT = "https://fcm.googleapis.com/fcm/send";

	public static void setNotification(String message, String token) throws ClientProtocolException, IOException {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		RequestConfig config = RequestConfig
				.custom()
				.setConnectTimeout(CONNECTION_TIMEOUT_MS)
				.setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
				.setSocketTimeout(CONNECTION_TIMEOUT_MS)
				.build();
		HttpPost request = new HttpPost(ENDPOINT);
		request.setConfig(config);
		request.addHeader("Content-Type", "application/json");
		request.addHeader("Authorization", "key="+SERVER_KEY);
		String msg = String.format(TEMPLATE, message, token);
		 System.out.println("Sending to " + ENDPOINT + ", "+ msg);
		request.setEntity(new StringEntity(msg));

		CloseableHttpResponse response = httpClient.execute(request);
		String xmlResp = EntityUtils.toString(response.getEntity());
		System.out.println(xmlResp);
	}

}
