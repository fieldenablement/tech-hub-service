package com.example.demo;

    
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

import javax.net.ssl.SSLContext;

import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;

import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Simple client that calls the WS-Security <code>GetFrequentFlyerMileage</code> operation using SAAJ and XWSS.
 *
 * @author Arjen Poutsma
 */
public class JobMgmtSeviceClient {

	private static final String GET_JOB = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:fiel=\"http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkJobManagementServiceRequestResponse_v1\">" 
			+ "   <soapenv:Header/> "
			+ "   <soapenv:Body> "
			+ "      <fiel:getJob>" 
			+ "         <inputHeader>"
			+ "            <systemSourceCd>7820</systemSourceCd>" 
			+ "            <userId>t835630</userId>"
			+ "            <requestDate>2019-03-19T11:00:00Z</requestDate>" 
			+ "         </inputHeader>"
			+ "         <jobAssignmentId>%s</jobAssignmentId>" 
			+ "      </fiel:getJob> "
			+ "   </soapenv:Body> "
			+ "</soapenv:Envelope>";
	
	private static final String UPDATE_JOB = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:fiel=\"http://xmlschema.tmi.telus.com/srv/RMO/ProcessMgmt/FieldWorkJobManagementServiceRequestResponse_v1\">" + 
			"   <soapenv:Header/>" + 
			"   <soapenv:Body>" + 
			"      <fiel:updateJob>" + 
			"         <inputHeader>" + 
			"            <systemSourceCd>7820</systemSourceCd>" + 
			"            <userId>T835630</userId>" + 
			"            <requestDate>2019-03-20T22:18:15.741Z</requestDate>" + 
			"         </inputHeader>" + 
			"          <jobId>%s</jobId>" + 
			"         <jobAssignmentId>%s</jobAssignmentId>" + 
			"         <teamWorkerId>%s</teamWorkerId>" + 
			"         <statusCd>ACCEPTED</statusCd>" + 
			"         <acceptDateTime>2019-03-20T22:18:15.741Z</acceptDateTime>" + 
			"      </fiel:updateJob>" + 
			"   </soapenv:Body>" + 
			"</soapenv:Envelope>";
	
	  private static final String KEYSTOREPATH = "/SDFCommon.jks"; 
	    private static final String KEYSTOREPASS = "secret";
	    private static final String ENDPOINT = "https://webservices.preprd.teluslabs.net/RMO/ProcessMgmt/FieldWorkJobManagementService_v1_0_vs1";
	    private static final int CONNECTION_TIMEOUT_MS = 60000;
	    
	    private KeyStore readStore() throws Exception {
	        try (InputStream keyStoreStream = this.getClass().getResourceAsStream(KEYSTOREPATH)) {
	            KeyStore keyStore = KeyStore.getInstance("JKS"); 
	            keyStore.load(keyStoreStream, KEYSTOREPASS.toCharArray());
	            return keyStore;
	        }
	    }	
	    
	    public String acceptJob(String jobId, String jobAssignmentid, String teammemberId) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, Exception {
	    	return performSimpleClientRequest(String.format(UPDATE_JOB, jobId, jobAssignmentid, teammemberId));
	    }
	    
	    public Customer getCustomerName(String jobAssignmentid) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, Exception {
	    	String xmlStr = performSimpleClientRequest(String.format(GET_JOB, jobAssignmentid));
	    	Document doc = convertStringToXMLDocument( xmlStr );
	    	NodeList nodeList = doc.getElementsByTagName("customerName");
	    	if (nodeList != null && nodeList.getLength() > 0) {
	    		Customer customer = new Customer(nodeList.item(0).getTextContent());
	    		return customer;
	    	}
	    	return null;
	    }
	    
	    private static Document convertStringToXMLDocument(String xmlString) throws ParserConfigurationException, SAXException, IOException
	    {
	        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder builder = null;
	        builder = factory.newDocumentBuilder();
	        Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
	        return doc;
	    }
	    
	    private String performSimpleClientRequest(String xml) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, Exception  {
	        SSLContext sslContext = SSLContexts.custom()
	                .loadKeyMaterial(readStore(),KEYSTOREPASS.toCharArray()) // use null as second param if you don't have a separate key password
	                .build();

	        System.out.println("Sending to " + ENDPOINT + ", "+ xml);
	        /*
	        HttpClient httpClient = HttpClients.custom().setSSLContext(sslContext).build();
	  	  HttpPost request = new HttpPost(ENDPOINT);
	  	  request.addHeader("Content-Type", "text/xml;charset=UTF-8");
    	  request.setEntity(new StringEntity(xml));
    
	        HttpResponse response = httpClient.execute(request);

	        return EntityUtils.toString(response.getEntity());
	*/
	        CloseableHttpClient httpClient = HttpClientBuilder.create().setSSLContext(sslContext).build();
	        RequestConfig config = RequestConfig
	        	      .custom()
	        	      .setConnectTimeout(CONNECTION_TIMEOUT_MS)
	        	      .setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
	        	      .setSocketTimeout(CONNECTION_TIMEOUT_MS)
	        	      .build();
	        	  HttpPost request = new HttpPost(ENDPOINT);
	        	  request.setConfig(config);
	        	  request.addHeader("Content-Type", "text/xml;charset=UTF-8");
	        	  request.setEntity(new StringEntity(xml));
	        
	        CloseableHttpResponse response = httpClient.execute(request);
	        String xmlResp = EntityUtils.toString(response.getEntity());
	        System.out.println(xmlResp);
	        return xmlResp;
	    }
	    
	    public static void main(String[] args) {
	    	JobMgmtSeviceClient client = new JobMgmtSeviceClient();
	    	String resp;
			try {
				resp = client.performSimpleClientRequest(String.format(GET_JOB, "123456"));
		    	System.out.println(resp);
		    	System.out.println(client.getCustomerName("85119735").getName());
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnrecoverableKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	    	
	    }
}

