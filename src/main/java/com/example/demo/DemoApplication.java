package com.example.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.concurrent.ExecutionException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class DemoApplication {

	private static final String FIRESTOR_PROJECT_ID = "techhubpoc";

	private FirestoreSvc firestoreSvc = null;
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);

	}

	private FirestoreSvc getFirestoreSvc() throws FileNotFoundException, IOException {
		if (this.firestoreSvc == null) {
			//this.firestoreSvc = new FirestoreSvc(FIRESTOR_PROJECT_ID);
			this.firestoreSvc = new FirestoreSvc();
		}
		
		return this.firestoreSvc;
	}
	
	private JobMgmtSeviceClient getJobMgmtSeviceClient() throws FileNotFoundException, IOException {
		return this.getFirestoreSvc().getJobMgmtSeviceClient();
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/assignment/{id}")
	public Customer getAssignment(@PathVariable String id) {
		try {
			return this.getJobMgmtSeviceClient().getCustomerName(id);
		} catch (Exception e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			return new Customer(sw.toString());
		}
	}

	@CrossOrigin(origins = "*")
	@PutMapping("/assignment/{id}")
	public Assignment updateAssignment(@RequestBody Assignment assignment, @PathVariable String id) {
		try {
			this.getFirestoreSvc().add(assignment);
		} catch (InterruptedException | ExecutionException | IOException e) {
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			assignment.setRemarkTxt(sw.toString());
		}
		return assignment;
	}
	
	@CrossOrigin(origins = "*")
	@PostMapping("/token")
	public String receiveClientToken(@RequestBody TokenRequest token) throws FileNotFoundException, IOException {
		System.out.println("Received Token" + token.getToken());
		getFirestoreSvc().addNewClientMessageToken(token.getToken());
		return "{\"RequestStatusCd\": \"SUCCESS\"}";
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping("/cleartoken")
	public String clearToken() throws FileNotFoundException, IOException {
		getFirestoreSvc().clearToken();
		return "{\"RequestStatusCd\": \"SUCCESS\"}";
	}
	
	
	@CrossOrigin(origins = "*")
	@GetMapping("/")
	public String hello() {
		String s = this.getAssignment("85119735").getName();
		return "Assignment id '85119735' in PT02 has customer name: '" + s +"'";
	}

}