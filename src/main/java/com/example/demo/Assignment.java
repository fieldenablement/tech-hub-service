package com.example.demo;
public class Assignment {

	private String assigmentId; 
	private String jobId; 
	private String teamWorkerId;
	private String workOrderId;
	private String locationAddressTxt;
	private String jobTypeCd;
	private String productCategoryCd;
	private String classificationCd;
	private String serviceClassCd;
	private String statusCd; 
	private String remarkTxt;
	private com.google.cloud.Timestamp start;
	private com.google.cloud.Timestamp end;

	public String getAssigmentId() {
		return assigmentId;
	}

	public void setAssigmentId(String assigmentId) {
		this.assigmentId = assigmentId;
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getTeamWorkerId() {
		return teamWorkerId;
	}

	public void setTeamWorkerId(String teamWorkerId) {
		this.teamWorkerId = teamWorkerId;
	}

	public String getWorkOrderId() {
		return workOrderId;
	}

	public void setWorkOrderId(String workOrderId) {
		this.workOrderId = workOrderId;
	}

	public String getLocationAddressTxt() {
		return locationAddressTxt;
	}

	public void setLocationAddressTxt(String locationAddressTxt) {
		this.locationAddressTxt = locationAddressTxt;
	}

	public String getJobTypeCd() {
		return jobTypeCd;
	}

	public void setJobTypeCd(String jobTypeCd) {
		this.jobTypeCd = jobTypeCd;
	}

	public String getProductCategoryCd() {
		return productCategoryCd;
	}

	public void setProductCategoryCd(String productCategoryCd) {
		this.productCategoryCd = productCategoryCd;
	}

	public String getClassificationCd() {
		return classificationCd;
	}

	public void setClassificationCd(String classificationCd) {
		this.classificationCd = classificationCd;
	}

	public String getServiceClassCd() {
		return serviceClassCd;
	}

	public void setServiceClassCd(String serviceClassCd) {
		this.serviceClassCd = serviceClassCd;
	}

	public String getStatusCd() {
		return statusCd;
	}

	public void setStatusCd(String statusCd) {
		this.statusCd = statusCd;
	}

	public String getRemarkTxt() {
		return remarkTxt;
	}

	public void setRemarkTxt(String remarkTxt) {
		this.remarkTxt = remarkTxt;
	}

	public com.google.cloud.Timestamp getStart() {
		return start;
	}

	public void setStart(com.google.cloud.Timestamp start) {
		this.start = start;
	}

	public com.google.cloud.Timestamp getEnd() {
		return end;
	}

	public void setEnd(com.google.cloud.Timestamp end) {
		this.end = end;
	}
	
}
